require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser');
const app = express()
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require("./configs/Db");

const UserRouter = require("./routers/User");
const EventRouter = require("./routers/Event");
const GroupRouter = require("./routers/Group");
const discussionRouter = require("./routers/Discussions");
const photoBookRouter = require("./routers/photoBook");
const SondageRouter = require("./routers/Sondages");
const ticketRouter = require("./routers/Tickets");

app.use("/user", UserRouter);
app.use("/event", EventRouter);
app.use("/group", GroupRouter);
app.use("/discussion", discussionRouter);
app.use("/photoBook", photoBookRouter);
app.use("/sondage", SondageRouter);
app.use("/ticket", ticketRouter);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});