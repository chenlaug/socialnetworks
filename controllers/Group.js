const Group = require('../models/Group');
const Event = require('../models/Event');

exports.createGroup = async (req, res) => {
  const id = req.user.id;
  try {
    const newGroup = { ...req.body, organizers: id };
    const group = new Group(newGroup);
    await group.save();
    res.status(201).json({ message: "Group created successfully", group });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};


exports.createEvent = async (req, res) => {
  const id = req.params.id;
  const userId = req.user.id;
  try {
    const group = await Group.findById(id);
    if (!group) {
      return res.status(404).json({ message: "Group not found" });
    }

    if (!group.canCreateEvents) {
      return res.status(403).json({ message: "You are not allowed to create events" });
    }

    if (!group.organizers.includes(userId) && !group.members.includes(userId)) {
      return res.status(403).json({ message: "You are not allowed to create events" });
    }

    const newEvent = { ...req.body, organizers: group.organizers, members: group.members };
    const event = new Event(newEvent);
    await event.save();
    res.status(201).json({ message: "Event created successfully", event });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.inviteMembers = async (req, res) => {
  const id = req.params.id;
  const userId = req.user.id;
  const { memberId } = req.body;

  try {
    const group = await Group.findById(id);
    if (!group) {
      return res.status(404).json({ message: "Group not found" });
    }
    if (!group.organizers.includes(userId)) {
      return res.status(403).json({ message: "You are not allowed to invite members" });
    }

    if (group.members.includes(memberId)) {
      return res.status(403).json({ message: "Member already in the group" });

    }
    group.members.push(memberId);
    await group.save();
    res.status(200).json({ message: "Member invited successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });

  }
}

exports.removeMembers = async (req, res) => {
  const id = req.params.id;
  const userId = req.user.id;
  const { memberId } = req.body;

  try {
    const group = await Group.findById(id);
    if (!group) {
      return res.status(404).json({ message: "Group not found" });
    }
    if (!group.organizers.includes(userId)) {
      return res.status(403).json({ message: "You are not allowed to remove members" });
    }

    if (!group.members.includes(memberId)) {
      return res.status(403).json({ message: "Member not in the group" });
    }

    group.members.pull(memberId);
    await group.save();
    res.status(200).json({ message: "Member removed successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });

  }
}

exports.getAllGroups = async (req, res) => {
  try {
    const groups = await Group.find({ typeGroup: { $in: ["public"] } })
    if (!groups) {
      return res.status(404).json({ message: "No group found" });
    }

    res.status(200).json(groups);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.getMyGroups = async (req, res) => {
  const id = req.user.id;
  try {
    const groups = await Group.find({ $or: [{ organizers: id }, { members: id }] });
    if (!groups) {
      return res.status(404).json({ message: "No group found" });
    }

    res.status(200).json(groups);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }

}


exports.getGroup = async (req, res) => {
  const id = req.params.id;
  const userId = req.user.id;
  try {
    const group = await Group.findById(id)
    if (!group) {
      return res.status(404).json({ message: "Group not found" });
    }
    if (!group.organizers.includes(userId) && !group.members.includes(userId)) {
      return res.status(403).json({ message: "You are not a member of this group" });
    }

    res.status(200).json(group);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.deleteGroup = async (req, res) => {
  const id = req.params.id;
  try {
    const group = await Group.findByIdAndDelete(id);
    if (!group) {
      return res.status(404).json({ message: "Group not found or already deleted" });
    }
    res.status(200).json({ message: "Group deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

exports.updateGroup = async (req, res) => {
  const id = req.params.id;
  try {
    const group = await Group.findByIdAndUpdate(id, req.body)
    if (!group) {
      return res.status(404).json({ message: "Group not found" });
    }
    await group.save();
    res.status(200).json({ message: "Group updated successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};