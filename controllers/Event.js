const Event = require('../models/Event');

exports.createEvent = async (req, res) => {
  try {
    const newEvent = { ...req.body, organizers: req.user.id };
    const event = new Event(newEvent);
    await event.save();
    res.status(201).json({ message: "Event created successfully", event });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.inviteMember = async (req, res) => {
  const id = req.params.id;
  const userIds = Array.isArray(req.body.userIds) ? req.body.userIds : [req.body.userIds];
  try {
    const event = await Event.findById(id);
    if (!event) {
      return res.status(404).json({ message: "Event not found" });
    }
    if (!event.organizers.includes(req.user.id)) {
      return res.status(401).json({ message: "You are not authorized to invite members to this event" });
    }
    const members = event.members;
    userIds.forEach(userId => {
      if (!members.includes(userId)) {
        members.push(userId);
      }

    });

    const updatedEvent = await Event.findByIdAndUpdate(id, { members }, { new: true });
    return res.status(200).json({ message: "Members invited successfully", event: updatedEvent });
  } catch (error) {
    res.status(500).json({ message: error.message });

  }
}

exports.removeMember = async (req, res) => {
  const id = req.params.id;
  const userId = req.body.userId;
  try {
    const event = await Event.findById(id);
    if (!event) {
      return res.status(404).json({ message: "Event not found" });
    }
    if (!event.organizers.includes(req.user.id)) {
      return res.status(401).json({ message: "You are not authorized to remove members from this event" });
    }
    const members = event.members.includes(userId);
    if (!members) {
      return res.status(404).json({ message: "User not found in event" });
    }
    const updatedMembers = await Event.findByIdAndUpdate(id, { $pull: { members: userId } }, { new: true });
    return res.status(200).json({ message: "Member removed successfully", event: updatedMembers });
  } catch (error) {
    res.status(500).json({ message: error.message });

  }
};

exports.getAllEvents = async (req, res) => {
  try {
    const events = await Event.find().populate('organizers').populate('members');
    res.status(200).json(events);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.getAllMyEvents = async (req, res) => {
  const id = req.user.id;
  try {
    const events = await Event.find({ organizers: id } || { members: id }).populate('organizers').populate('members');
    res.status(200).json(events);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

exports.getEvent = async (req, res) => {
  const id = req.params.id;
  try {
    const event = await Event.findById(id).populate('organizers').populate('members');
    res.status(200).json(event);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};


exports.deleteEvent = async (req, res) => {
  const id = req.params.id;
  const userId = req.user.id;
  try {
    const event = await Event.findById(id);
    if (!event) {
      return res.status(404).json({ message: "Event not found" });
    }

    if (!event.organizers.includes(userId)) {
      return res.status(401).json({ message: "You are not authorized to delete this event" });
    }
    await Event.findByIdAndDelete(id);
    res.status(200).json({ message: "Event deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

exports.updateEvent = async (req, res) => {
  const id = req.params.id;
  const userId = req.user.id;

  try {
    const event = await Event.findById(id)
    if (!event) {
      return res.status(404).json({ message: "Event not found" });
    }
    if (!event.organizers.includes(userId)) {
      return res.status(401).json({ message: "You are not authorized to update this event" });
    }

    const updatedEvent = await Event.findByIdAndUpdate(id, req.body, { new: true });
    res.status(200).json({ message: "Event updated successfully", event: updatedEvent });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};