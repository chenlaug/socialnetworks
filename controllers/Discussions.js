const Discussion = require('../models/Discussions');

exports.sendMessage = async (req, res) => {
  const userId = req.user.id;
  try {
    let allowedToSendMessage = false;

    if (req.type === 'Group') {
      const group = req.groupOrEvent;
      const isMember = group.members.some(member => member.equals(userId));
      const isOrganizer = group.organizers.some(organizer => organizer.equals(userId));
      allowedToSendMessage = isMember || isOrganizer;
    } else if (req.type === 'Event') {
      const event = req.groupOrEvent;
      console.log(event.members);
      console.log(event.organizers);
      const isParticipant = event?.members.some(member => member.equals(userId));
      const isOrganizer = event.organizers.some(organizer => organizer.equals(userId));
      allowedToSendMessage = isParticipant || isOrganizer;
    }

    if (!allowedToSendMessage) {
      return res.status(403).json({ message: "You are not authorized to send messages in this group or event" });
    }

    const discussion = new Discussion({
      group: req.type === 'Group' ? req.groupOrEvent._id : null,
      event: req.type === 'Event' ? req.groupOrEvent._id : null,
      messages: [{
        content: req.body.content,
        author: userId
      }]
    });

    await discussion.save();
    res.status(201).json({ message: "Message sent successfully", discussion: discussion });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: error.message });
  }
};


exports.writeComment = async (req, res) => {
  const userId = req.user.id;
  const discussionId = req.params.id;
  const { messageId, content } = req.body;

  try {
    const discussion = await Discussion.findById(discussionId)
      .populate('group')
      .populate('event');

    if (!discussion) {
      return res.status(404).json({ message: "Discussion not found" });
    }

    const parent = discussion.group || discussion.event;
    if (!parent) {
      return res.status(404).json({ message: "No associated group or event found for this discussion" });
    }

    const isMember = parent.members && parent.members.some(member => member.equals(userId));
    const isOrganizer = parent.organizers && parent.organizers.some(organizer => organizer.equals(userId));

    if (!isMember && !isOrganizer) {
      return res.status(403).json({ message: "You are not authorized to comment in this discussion" });
    }

    const message = discussion.messages.id(messageId);
    if (!message) {
      return res.status(404).json({ message: "Message not found in the discussion" });
    }

    message.comment.push({
      content: content,
      author: userId
    });

    await discussion.save();
    res.status(201).json({ message: "Comment added successfully", comment: { content, author: userId } });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
