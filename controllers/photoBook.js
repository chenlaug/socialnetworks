const photoBook = require('../models/photoBook');

exports.createPhotoBook = async (req, res) => {
  const eventId = req.params.Id;
  const userId = req.user.id;
  const { url } = req.body;
  console.log("Received URL:", url);
  try {
    const newPhotoBook = { event: eventId, photos: [{ url: url, author: userId }] };
    const photo = new photoBook(newPhotoBook);
    await photo.save();
    res.status(201).json({ message: "photo created successfully", photo });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.addComment = async (req, res) => {
  const photobookID = req.params.id;
  const userId = req.user.id;
  const { content, photoId } = req.body;

  try {
    const photobook = await photoBook.findById(photobookID);
    if (!photobook) {
      return res.status(404).json({ message: "PhotoBook not found" });
    }

    const photo = photobook.photos.id(photoId);
    if (!photo) {
      return res.status(404).json({ message: "Photo not found" });
    }

    photo.comments.push({ content: content, author: userId });

    await photobook.save();
    res.status(201).json({ message: "Comment added successfully", photo });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
