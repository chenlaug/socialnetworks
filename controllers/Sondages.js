const Sondage = require('../models/Sondages');

exports.createSondage = async (req, res) => {
  try {
    const sondage = new Sondage(req.body);
    await sondage.save();
    res.status(201).json({ message: "Sondage created successfully", sondage });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.getAllSondages = async (req, res) => {
  try {
    const sondages = await Sondage.find()
    res.status(200).json(sondages);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.getSondage = async (req, res) => {
  const id = req.params.id;
  try {
    const sondage = await Sondage.findById(id)
    res.status(200).json(sondage);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.deleteSondage = async (req, res) => {
  const id = req.params.id;
  try {
    const sondage = await Sondage.findByIdAndDelete(id);
    if (!sondage) {
      res.status(404).json({ message: "Sondage not found" });
    }

    res.status(200).json({ message: "Sondage deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.updateSondage = async (req, res) => {
  const id = req.params.id;
  try {
    const sondage = await Sondage.findByIdAndUpdate(id, req.body, { new: true });

    if (!sondage) {
      res.status(404).json({ message: "Sondage not found" });
    }

    res.status(200).json({ message: "Sondage updated successfully", sondage });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};