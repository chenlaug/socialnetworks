const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const comentaireShema = new Schema({
  content: { type: String, required: true },
  author: { type: Schema.Types.ObjectId, ref: 'User' }
});

const photoShema = new Schema({
  url: { type: String, required: true },
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  comments: [comentaireShema]
});

const photoBookShema = new Schema({
  event: { type: Schema.Types.ObjectId, ref: 'Event' },
  photos: [photoShema]
}, { timestamps: true });

const PhotoBook = mongoose.model('PhotoBook', photoBookShema);
module.exports = PhotoBook;