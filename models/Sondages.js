const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const answerShema = new Schema({
  content: { type: String, required: true },
  author: { type: Schema.Types.ObjectId, ref: 'User' }
})

const questionShema = new Schema({
  content: { type: String, required: true },
  answers: [answerShema]
})

const sondageShema = new Schema({
  event: { type: Schema.Types.ObjectId, ref: 'Event' },
  question: [questionShema]
}, {
  timestamps: true
});

const Sondage = mongoose.model('Sondage', sondageShema);
module.exports = Sondage;