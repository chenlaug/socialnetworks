const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const eventSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  startDate: { type: Date, required: true },
  endDate: { type: Date, required: true },
  location: { type: String, required: true },
  coverPhoto: { type: String, default: 'https://img.freepik.com/free-photo/volume-scene-poster-nobody-room_1258-261.jpg' },
  private: { type: Boolean, default: false },
  organizers: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  members: [{ type: Schema.Types.ObjectId, ref: 'User', }],
}, { timestamps: true });

const Event = mongoose.model('Event', eventSchema);
module.exports = Event;