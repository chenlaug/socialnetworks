const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;

const userShema = new Schema({
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  email: { type: String, required: true, unique: true, lowercase: true },
  password: { type: String, required: true },
  age: { type: Number, required: true },
  city: { type: String, required: true },
  city_code: { type: Number, required: true },
  street_number: { type: Number, required: true },
  street_type: { type: String },
  street_name: { type: String, required: true },
  phone: { type: String, required: true },
  image: { type: String, default: 'https://pbs.twimg.com/profile_images/1126137112825335808/L5WvNz8W_400x400.jpg' },
}, { timestamps: true });

userShema.pre('save', async function (next) {
  const user = this;
  try {
    if (!user.isModified('password')) return next();
    const hash = await bcrypt.hash(user.password, 10);
    user.password = hash;
    next();
  } catch (error) {
    next(error);
  }
});

const User = mongoose.model('User', userShema);
module.exports = User;