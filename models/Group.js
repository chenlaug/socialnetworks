const express = require('express');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const groupSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  icon: { type: String, default: 'https://d1csarkz8obe9u.cloudfront.net/posterpreviews/heart-icon-design-template-7582e2dfe68c75576eeec72967453753_screen.jpg?ts=1624545175' },
  coverPhoto: { type: String, default: 'https://img.freepik.com/free-photo/volume-scene-poster-nobody-room_1258-261.jpg' },
  typeGroup: { type: String, enum: ['public', 'private', "secret"], default: 'public', },
  canPublish: { type: Boolean, default: true },
  canCreateEvents: { type: Boolean, default: true },
  organizers: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  members: [{ type: Schema.Types.ObjectId, ref: 'User', }],
}, { timestamps: true });

const Group = mongoose.model('Group', groupSchema);
module.exports = Group;