const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const typeSchema = new Schema({
  name: { type: String, required: true },
  price: { type: Number, required: true },
  quantity: { type: Number, required: true }
});

const ticketSchema = new Schema({
  event: { type: Schema.Types.ObjectId, ref: 'Event' },
  type: [typeSchema],
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  datePurchase: { type: Date, default: Date.now },
});

const Ticket = mongoose.model('Ticket', ticketSchema);
module.exports = Ticket;