const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentSchema = new Schema({
  content: { type: String, required: true },
  author: { type: Schema.Types.ObjectId, ref: 'User' },
}, { timestamps: true });

const messageSchema = new Schema({
  content: { type: String, required: true },
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  comment: [commentSchema],
}, { timestamps: true });

const discussionSchema = new Schema({
  group: { type: Schema.Types.ObjectId, ref: 'Group' },
  event: { type: Schema.Types.ObjectId, ref: 'Event' },
  messages: [messageSchema]
}, { timestamps: true });

discussionSchema.pre('validate', function (next) {
  const isGroupSet = this.group !== null && this.group !== undefined;
  const isEventSet = this.event !== null && this.event !== undefined;

  if (!isGroupSet && !isEventSet) {
    return next(new Error('A discussion must be linked to either a group or an event.'));
  } else if (isGroupSet && isEventSet) {
    return next(new Error('A discussion cannot be linked to both a group and an event.'));
  }
  next();
});


const Discussion = mongoose.model('Discussion', discussionSchema);
module.exports = Discussion;
