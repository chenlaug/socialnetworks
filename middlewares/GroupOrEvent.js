const Group = require('../models/Group');
const Event = require('../models/Event');

const groupOrEvent = async (req, res, next) => {
  const { id } = req.params;
  try {
    const group = await Group.findById(id);
    if (group) {
      req.groupOrEvent = group;
      req.type = 'Group';
      return next();
    }

    const event = await Event.findById(id);
    if (event) {
      req.groupOrEvent = event;
      req.type = 'Event';
      return next();
    }

    res.status(404).send({ message: 'No group or event found with the provided ID' });
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
}

module.exports = groupOrEvent;