const express = require('express');
const router = express.Router();
const eventController = require('../controllers/Event');
const authenticateToken = require('../middlewares/Auth');


router.post('/create', authenticateToken, eventController.createEvent);
router.post('/invite/:id', authenticateToken, eventController.inviteMember);
router.post('/remove/:id', authenticateToken, eventController.removeMember);
router.get('/events', authenticateToken, eventController.getAllEvents);
router.get('/my-events', authenticateToken, eventController.getAllMyEvents);
router.get('/:id', authenticateToken, eventController.getEvent);
router.delete('/:id', authenticateToken, eventController.deleteEvent);
router.put('/:id', authenticateToken, eventController.updateEvent);

module.exports = router;
