const express = require('express');
const router = express.Router();
const SondagesController = require('../controllers/Sondages');

router.post('/create', SondagesController.createSondage);
router.get('/', SondagesController.getAllSondages);
router.get('/:id', SondagesController.getSondage);
router.delete('/:id', SondagesController.deleteSondage);
router.put('/:id', SondagesController.updateSondage);

module.exports = router;