const express = require('express');
const router = express.Router();
const discussionController = require('../controllers/Discussions');
const authenticateToken = require('../middlewares/Auth');
const groupOrEvent = require('../middlewares/GroupOrEvent');

router.post('/message/:id', authenticateToken, groupOrEvent, discussionController.sendMessage);
router.post('/comment/:id', authenticateToken, discussionController.writeComment);


module.exports = router;