const express = require('express');
const router = express.Router();
const photoBookController = require('../controllers/photoBook');
const authenticateToken = require('../middlewares/Auth');

router.post('/create/:id', authenticateToken, photoBookController.createPhotoBook);
router.post('/comment/:id', authenticateToken, photoBookController.addComment);

module.exports = router;
