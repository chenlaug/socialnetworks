const express = require('express');
const router = express.Router();
const ticketController = require('../controllers/Ticket');
const authenticateToken = require('../middlewares/Auth');

router.post('/create', authenticateToken, ticketController.createTicket);

module.exports = router;