const express = require('express');
const router = express.Router();
const userController = require('../controllers/User');
const authenticateToken = require('../middlewares/Auth');

router.post('/register', userController.register);
router.get('/', authenticateToken, userController.getAllUsers);
router.get('/:id', authenticateToken, userController.getUser);
router.delete('/', authenticateToken, userController.deleteUser);
router.put('/', authenticateToken, userController.updateUser);
router.post('/login', userController.login);

module.exports = router;
