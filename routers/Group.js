const express = require('express');
const router = express.Router();
const groupController = require('../controllers/Group');
const authenticateToken = require('../middlewares/Auth');


router.post('/create', authenticateToken, groupController.createGroup);
router.post('/invite/:id', authenticateToken, groupController.inviteMembers);
router.post('/remove/:id', authenticateToken, groupController.removeMembers);
router.get('/create-event/:id', authenticateToken, groupController.createEvent);
router.get('/groups', authenticateToken, groupController.getAllGroups);
router.get('/mygroups', authenticateToken, groupController.getMyGroups);
router.get('/:id', authenticateToken, groupController.getGroup);
router.delete('/:id', authenticateToken, groupController.deleteGroup);
router.put('/:id', authenticateToken, groupController.updateGroup);

module.exports = router;